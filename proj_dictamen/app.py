import os
import uuid
import boto3
import requests
import json
import pymysql
from botocore.exceptions import ClientError
from flask import Flask, request, jsonify
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager
from werkzeug.security import generate_password_hash, check_password_hash
from zappa.asynchronous import task


def db_connect():
    return pymysql.connect(  host=os.environ.get('db_host'), 
                                user=os.environ.get('db_user'), 
                                passwd=os.environ.get('db_passwd'), 
                                db=os.environ.get('db_dbname') )

app = Flask(__name__)
app.debug = True
app.config['JWT_SECRET_KEY'] = os.environ.get('jwt_secret_key')

jwt = JWTManager(app)

@app.route('/users', methods=['POST'])
def createUser():
    data = request.json
    if not (data["email"] and data["password"]):
        return jsonify({"status": "ERROR", "data": "Incomplete request"}), 400
    db_conn = db_connect()
    cur = db_conn.cursor()
    cur.execute("SELECT id from users where email = %s", (data["email"]))
    if cur.rowcount:
        return jsonify({"status": "ERROR", "data": "User already exist"}), 400
    cur.execute("INSERT INTO users(id, email, password) values(%s,%s,%s)", (str(uuid.uuid4()),data["email"],generate_password_hash(data["password"])))
    db_conn.commit()
    return jsonify({"status": "OK", "data": "User created"}), 201

@app.route('/users/login', methods=['POST'])
def loginUser():
    data = request.json
    if not (data["email"] and data["password"]):
        return jsonify({"status": "ERROR", "data": "Incomplete request"}), 400
    db_conn = db_connect()
    cur = db_conn.cursor()
    cur.execute("SELECT email,password,id from users where email = %s", (data["email"]))
    if not cur.rowcount:
        return jsonify({"status": "ERROR", "data": "User not found"}), 404
    record = cur.fetchone()
    if not check_password_hash(record[1], data["password"]):
        return jsonify({"status": "ERROR", "data": "User data not valid"}), 403
    access_token = create_access_token(identity=record[2])
    return jsonify(access_token=access_token), 200

@app.route("/comments", methods=['POST'])
@jwt_required()
def uploadFile():
    s3_client = boto3.client('s3')
    if 'file' not in request.files:
        return jsonify({"status": "ERROR", "data": "Not comments files"}), 400
    file = request.files['file']
    if not file:
        return jsonify({"status": "ERROR", "data": "Not comments files"}), 400
    if file.filename == '':
        return jsonify({"status": "ERROR", "data": "Not selected comments files"}), 400
    comment_id = str(uuid.uuid4())
    try:
        response = s3_client.upload_fileobj(file, os.environ.get('s3_bucket'), comment_id)
    except ClientError as e:
        return jsonify({"status": "ERROR", "data": str(e)}), 500
    db_conn = db_connect()
    cur = db_conn.cursor()
    cur.execute("INSERT INTO comments(id,status) values(%s,%s)", (comment_id, 0))
    sqs_client = boto3.client('sqs')
    sqs_client.send_message(QueueUrl=os.environ.get('sqs_url'),
                            MessageBody=comment_id)
    db_conn.commit()
    # analizyngComments(comment_id)
    return jsonify({"status": "OK", "data": "Comments created"}), 201


@app.route("/comments", methods=['GET'])
@jwt_required()
def getComments():
    db_conn = db_connect()
    cur = db_conn.cursor(pymysql.cursors.DictCursor)
    cur.execute("SELECT id, polarity, type, status, date_entered from comments order by date_entered desc")
    records = cur.fetchall()
    return jsonify(records), 200
    

@task
def analizyngComments(comment_id):
    s3_client = boto3.client('s3')
    data = s3_client.get_object(Bucket=os.environ.get('s3_bucket'), Key=comment_id)
    contents = data['Body'].read()   
    req = requests.post(os.environ.get('isentim_url'), 
                        headers={'accept': 'application/json', 'content-type': 'application/json'},
                        data=json.dumps({'text': contents.decode("utf-8")}))
    resp = req.json()
    db_conn = db_connect()
    cur = db_conn.cursor()
    cur.execute("UPDATE comments set polarity = %s, type = %s, status = %s where id = %s", (resp["result"]["polarity"], resp["result"]["type"], 1, comment_id))
    db_conn.commit()
    
if __name__ == '__main__':
 app.run()