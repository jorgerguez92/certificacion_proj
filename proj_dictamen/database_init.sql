CREATE TABLE users(
    id varchar(32) NOT NULL,
    email varchar(256) NOT NULL,
    password varchar(256) NOT NULL,
    PRIMARY KEY(id)
)

CREATE TABLE comments(
    id varchar(32) NOT NULL,
    polarity float(3,2),
    type varchar(50),
    status tinyint(4) NOT NULL DEFAULT 0,
    date_entered datetime NOT NULL DEFAULT utc_timestamp()
    PRIMARY KEY(id)
)