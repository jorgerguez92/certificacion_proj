import os
import json
import pymysql
import boto3
import requests

def lambda_handler(event, context):
    print(event)
    comment_id = event["Records"][0]["body"]
    db_conn = db_connect()
    cur = db_conn.cursor()
    cur.execute("SELECT * from comments where status = 1 and id = %s", (comment_id))
    if cur.rowcount:
        return { "statusCode": 200, "body": json.dumps({"status": "OK", "data": "Comment already evaluated"})}
    s3_client = boto3.client('s3')
    data = s3_client.get_object(Bucket=os.environ.get('s3_bucket'), Key=comment_id)
    contents = data['Body'].read()   
    req = requests.post(os.environ.get('isentim_url'), 
                        headers={'accept': 'application/json', 'content-type': 'application/json'},
                        data=json.dumps({'text': contents.decode("utf-8")}))
    if req.status_code is not 200:
        raise Exception("Error in isentim")
    resp = req.json()
    cur.execute("UPDATE comments set polarity = %s, type = %s, status = %s where id = %s", (resp["result"]["polarity"], resp["result"]["type"], 1, comment_id))
    db_conn.commit()
    return { "statusCode": 200, "body": json.dumps({"status": "OK", "data": "Comment already evaluated"})}
    
def db_connect():
    return pymysql.connect(  host=os.environ.get('db_host'), 
                                user=os.environ.get('db_user'), 
                                passwd=os.environ.get('db_passwd'), 
                                db=os.environ.get('db_dbname') )